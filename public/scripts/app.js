'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _IndecisionApp = require('./components/IndecisionApp');

var _IndecisionApp2 = _interopRequireDefault(_IndecisionApp);

require('./styles/styles.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_reactDom2.default.render(_react2.default.createElement(_IndecisionApp2.default, { options: ['Sleep', 'Zouk'] }), document.getElementById('app'));
/* 
class Header extends React.Component {
    
    render() {
      return (
        <div>
       
          <h1>{this.props.title}</h1>
          <h2>{this.props.subtitle  }</h2>
        </div>
      );
    }
  } */

/*   
  class Action extends React.Component {
    render() {
      return (
        <div>
          <button disabled={this.props.hasOptions} onClick={this.props.handlePick}>What should I do?</button>
        </div>
      );
    }
  } */

/*   class Options extends React.Component {
    render() {
      return (
        <div>
            <button onClick={this.props.handleDeleteOptions}>Remove all</button>
            { 
                this.props.options.map((value,index) => <Option key={index} id={index} valueOption={value}></Option>)
            }
        </div>
      );
    }
  } */

/* class Option extends React.Component{
    
    render(){
        return(
            <div>
            <li key={this.props.id}>{this.props.valueOption}</li>
            </div>
        );
    }
} */

/*   const jsx = (
    <div>
      <IndecisionApp />
    </div>
  ); */
/* 
  const User = () =>{
      return(
          <div>
            <p>Name: </p>
            <p> Age: </p>
          </div>
      );
  }; */

/* const t = <p> Testing</p>;
  ReactDOM.render(t, document.getElementById('app')); */
