const person = {
    name: 'Roberto',
    age: 30,
    location:{
        city: 'Vitoria',
        temp: 25
    }
}


const book = {
    title: "Trilogia do Graal",
    author: 'Ryan Holiday',
    publisher:{
        name: 'Peguin'
    }
}

const {name: publisherName = 'No publisher'} = book.publisher;
console.log(publisherName);



const item =['coffe','$2.00','$2.50','$3.00'];

const [itemName, , mediumPrice, ] = item;

console.log(`A medium ${itemName} costs ${mediumPrice}`);