import { createStore } from 'redux';

const incrementCount = (/*payload = {}*/{ value: incrementBy = 1} = {}) => ({ 
    type: 'INCREMENT',
    //incrementBy: typeof payload.incrementBy === 'number' ? payload.incrementBy : 1
    incrementBy: incrementBy
});


const decrementCount = (/*payload = {}*/{ value: decrementBy = 1} = {}) => ({ 
    type: 'DECREMENT',
    //incrementBy: typeof payload.incrementBy === 'number' ? payload.incrementBy : 1
    decrementBy: decrementBy
});

const resetCount = (/*payload = {}*/{ value: resetBy = 0} = {}) => ({ 
    type: 'RESET',
    //incrementBy: typeof payload.incrementBy === 'number' ? payload.incrementBy : 1
    resetBy: resetBy
});

const setCount = ({ value: setBy = 101} = {}) => ({ 
    type: 'RESET',
    resetBy: setBy
});

//reducers
const countReducer = (state = {count: 0}, action) =>{
    switch(action.type){
        case 'INCREMENT':
             return{
                count: state.count + action.incrementBy
             };
        case 'DECREMENT':
        //const decrementBy = typeof action.decrementBy === 'number' ? action.decrementBy : 1;
            return{
                count: state.count + action.decrementBy
            };
        case 'RESET':
            return{
                count: action.resetBy
            };
        case 'SET':
            return{
                count: action.setBy
            };
        default:
             return state;

    }
    return state.count;
};

const store = createStore(countReducer);     
/* 
alert(store.getState());

this.setState((prevState) => {
    return prevState;
}); */

store.subscribe(() => {
    console.log(store.getState());
    
});


store.dispatch(incrementCount({value:5}));

store.dispatch(incrementCount());

store.dispatch(decrementCount());

store.dispatch(decrementCount({value:10}));

store.dispatch(resetCount());
/* 
store.dispatch({
    type : 'DECREMENT',
    decrementBy: 10
});
 */

/* store.dispatch({
    type : 'DECREMENT'
});
 */
/* store.dispatch({
    type : 'RESET'
}); */

store.dispatch(decrementCount());

/* store.dispatch({
    type : 'SET'
}); */

store.dispatch(setCount());