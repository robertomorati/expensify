import expenses from '../fixtures/expenses';
import selectExpensesTotal from '../../selectors/expenses-total.js';

test('should return 0 if no expenses', () => {
    const res = selectExpensesTotal([]);
    expect(res).toBe(0);
});
  

test('should return 100 if has one expense', () => {
    const res = selectExpensesTotal([expenses[0]]);
    expect(res).toBe(100);
});


test('should return 5100 for expenses', () => {
    const res = selectExpensesTotal(expenses);
    expect(res).toBe(5100);
});
  