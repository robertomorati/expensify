const moment = require.requireActual('moment');

export default (timestamp = 1) => {
  return moment(timestamp);
};
