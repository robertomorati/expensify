import moment from 'moment';
import { setStartDate, setEndDate, setTextFilter, sortByAmount, sortByDate } from '../../actions/filters';


test('Generate set start date action object', () => {
    const action = setStartDate(moment(0));

    expect(action).toEqual({
        type: 'SET_START_DATE',
        startDate: moment(0)
    });
});


test('Generate set end  action object', () => {
    const action = setEndDate(moment(0));

    expect(action).toEqual({
        type: 'SET_END_DATE',
        endDate: moment(0)
    });
});


test('Generate set text filter action with text value', () => {
    const action = setTextFilter('Something');

    expect(action).toEqual({
        type: 'SET_TEXT_FILTER',
        text: 'Something'
    });
});


test('Generate set text filter action with default', () => {
    const action = setTextFilter();

    expect(action).toEqual({
        type: 'SET_TEXT_FILTER',
        text: ''
    });
});


test('Generate action object for sort by date', () => {
    const action = setTextFilter();

    expect(sortByDate()).toEqual({
        type: 'SORT_BY_DATE'
    });
});

test('Generate action object for sort by amount', () => {
   
    expect(sortByAmount()).toEqual({
        type: 'SORT_BY_AMOUNT',
    });
});


