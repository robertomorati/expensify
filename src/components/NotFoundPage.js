import React from 'react';
import { Link } from 'react-router-dom';

const NotFoundPage = () => (
    <div>
     404! - <Link to="/">Go home</Link>
     404! - <a href="/">Go home Server</a>
    </div>
);

export default NotFoundPage;
  