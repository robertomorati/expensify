import moment from 'moment';

const getTotalExpenses = (expenses) =>{

    let total = 0.0;

    for (var key in expenses) {
        if (expenses.hasOwnProperty(key)) {
           total = total + expenses[key].amount;
        }
     }
    return total;
};


export default getTotalExpenses;