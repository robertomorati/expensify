console.log("App js i running!");


const app = {
  title: "Indecision App",
  subtitle: "This is for something..",
  options: [],
  //printOptionsOld() { return this.options.map(function(value, index) {return <li key="value">{value}</li>})},
  printOptions() { return this.options.map((value,index) => <li key={index}>{value}</li>)}

};

const onFormSubmit = (e) => {
  e.preventDefault();

  const option = e.target.elements.option.value;

  if (option){
    console.log(option);
    app.options.push(option);
    e.target.elements.option.value = '';
    render();
  }

};

const onMakeDecision = () =>{ 
   const randoumNum = Mathe.floor(Math.random() * app.options.length);
   const option =   app.options[randoumNum];
}

const onRemoveAll = (e) => {
  e.preventDefault();
  app.options = [];
  render();

};
const appRoot = document.getElementById('app');

const render = () => {
  const template =( 
    <div>
      <h1>{app.title}</h1> 
      {app.subtitle ? <p>{app.subtitle}</p>: <p>No subtitle</p>}
      <button disabled={ app.options.length === 0 } onClick={onMakeDecision}>What should I do?</button>
      <button onClick={onRemoveAll}>Remove all options</button>
      <p>The options are:</p>
      { app.options.length > 0 ?
        <ol>
        {app.printOptions()}
        </ol>
      : 'The are no options.'
      }
      
      <form onSubmit={onFormSubmit}>
        <input type="text" name="option"/>
        <button>Add option</button>
        
      </form>
    </div>
  );
  ReactDOM.render(template, appRoot);
};


render();


/**
let count = 0;
const addOne = () => {count = count + 1; renderCounterApp()};
const renderCounterApp = () => {
  const templateTwo = (
    <div>
      <h1>Count: {count}</h1>
      <button onClick={addOne}>+1</button>
    </div>
  );
  ReactDOM.render(template, appRoot);
}
  
renderCounterApp();
 */