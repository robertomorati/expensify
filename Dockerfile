FROM node:9.11.1 as build

RUN mkdir /usr/src/app
RUN mkdir /usr/src/app/expensify
WORKDIR /usr/src/app/expensify

ENV PATH /usr/src/app/expensify/node_modules/.bin:$PATH

COPY package.json /usr/src/app/expensify/package.json

COPY .  /usr/src/app/expensify/
